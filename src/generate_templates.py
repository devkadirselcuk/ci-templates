#!/bin/env python3

import yaml
import re
import sys
import tempfile

from pathlib import Path

sys.path.insert(0, './tools')
import ci_fairy  # noqa


def generate_template(configs, dest, template):
    '''
    Invoke ci-fairy generate-template with the given arguments.

    .. param:: configs
        An array of tuples of type (path, rootnode).

    '''
    config_args = []
    for config, root in configs:
        config_args += ['--config', f'{config}']
        if root is not None:
            config_args += ['--root', f'{root}']

    args = ['generate-template'] + config_args + ['--output-file', f'{dest}', f'{template}']
    ci_fairy.main(args, standalone_mode=False)


if __name__ == '__main__':
    src_folder = Path('src')
    config_folder = src_folder / 'config'
    ci_folder = Path('.gitlab-ci')
    ci_folder.mkdir(exist_ok=True)
    globals_file = config_folder / 'ci-globals.yml'

    def config_files():
        for f in config_folder.iterdir():
            if f.name.endswith('.yml') and f.name != globals_file.name:
                yield f

    for cfile in config_files():
        name = cfile.name[:-4]  # drop .yml

        template = src_folder / f'{name}.tmpl'
        template_ci = src_folder / f'{name}-ci.tmpl'
        if not template.exists() and not template_ci.exists():
            template = src_folder / 'distribution-template.tmpl'
            template_ci = src_folder / 'distribution-template-ci.tmpl'

        config = [(f'{globals_file}', ''), (f'{cfile}', f'{name}')]
        if template.exists():
            # generate the distribution's template file,
            # e.g. /templates/alpine.yml
            dest = Path('templates') / f'{name}.yml'
            generate_template(configs=config, dest=dest, template=template)

        if template_ci.exists():
            # generate our CI file, e.g. /.gitlab-ci/alpine-ci.yml
            dest = ci_folder / f'{name}-ci.yml'
            generate_template(configs=config, dest=dest, template=template_ci)

    def template_files():
        for f in Path('templates').iterdir():
            if f.name.endswith('.yml') and f.name != 'ci-fairy.yml':
                yield f

    # We've generated all the templates, search for any
    # full image reference $registry/$path:$imagespec anywhere in your
    # templates and add that to the remote_images so we can test those
    # during the CI pipeline

    with open(globals_file) as fd:
        config = yaml.load(fd, Loader=yaml.Loader)
    registry, path = config['ci_templates_registry'], config['ci_templates_registry_path']
    pattern = re.compile(f'^[^#]+({registry}{path}:[\\w:.-]+)')
    remote_images = []
    for template in Path('templates/').glob('*.yml'):
        for line in open(template):
            matches = pattern.match(line)
            if matches:
                remote_images.append(matches[1])

    # write out a temporary config file with the remote images and the
    # distribution names. Use that to generate our .gitlab-ci.yml file
    with tempfile.NamedTemporaryFile() as new_config:
        s = '", "'.join(sorted(set(remote_images)))
        new_config.write(f'remote_images: ["{s}"]\n'.encode('utf8'))

        distributions = [d.name[:-4] for d in template_files()]
        s = '", "'.join(sorted(set(distributions)))
        new_config.write(f'distribs: ["{s}"]\n'.encode('utf8'))
        new_config.flush()

        # now generate the .gitlab-ci.yml file
        ci_fairy_file = config_folder / 'ci-fairy.yml'
        dest = Path('.gitlab-ci.yml')
        template = src_folder / 'gitlab-ci.tmpl'
        config = [(f'{globals_file}', ''),
                  (f'{ci_fairy_file}', 'ci-fairy'),
                  (f'{new_config.name}', '')]
        generate_template(configs=config, dest=dest, template=template)
